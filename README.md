# Tessellate Relatively

The tessellate-relatively plugin is a Pixyz Studio [module](https://www.pixyz-software.com/documentations/html/2020.1/studio/Plugins.html). Tessellate your model parts depending on the length of their bounding box (size). The plugin is accessible either from the **CAD** menu or from the **Plugins** menu.

## Required Software

![](doc/puce-studio-64x64.png)|
:-:|
Studio|

## Set up

* Clone or download the project at `C:/ProgramData/PiXYZStudio/plugins`
