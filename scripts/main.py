from collections import defaultdict

def point_sub(pt1, pt2):
    return geom.Point3(pt1.x - pt2.x, pt1.y - pt2.y, pt1.z - pt2.z)

def dot_product(pt1, pt2):
    return pt1.x * pt2.x + pt1.y * pt2.y + pt1.z * pt2.z

def vector_length(v):
    return math.sqrt(dot_product(v, v))

def get_size(occurrence):
    """
    Returns the bounding box diagonal size in mm
    """
    aabb = scene.getAABB([occurrence])
    return vector_length(point_sub(aabb.high, aabb.low))

def get_parts(occurrences):
    parts = list()
    for occ in occurrences:
        parts.extend(scene.getPartOccurrences(occ))
    return parts

def get_value(value, max_value):
    # value always positive, not max_value
    if max_value < 0:
        if value < 0: return -1
        else: return value
    else:
        if value < 0: return max_value
        else: return min(value, max_value)



def tessellate(occurrences, maxSag, ratioSag, maxLength, ratioLength, maxAngle, createNormals, uvMode, uvChannel, uvPadding, createTangents, createFreeEdges, keepBRepShape, overrideExistingTessellation):
    print('Tessellating relatively...')
    parts = get_parts(occurrences)
    core.pushProgression(len(parts))
    core.removeConsoleVerbose(2)
    tessellation_groups = defaultdict(list)
    for part in parts:
        size = get_size(part)
        sag = get_value(ratioSag * size, maxSag)
        length = get_value(ratioLength * size, maxLength)
        core.addCustomProperty(part, 'Sag', str(sag))
        core.addCustomProperty(part, 'Length', str(length))
        tessellation_groups[size].append(part)
    for size, group in tessellation_groups.items():
        sag = get_value(ratioSag * size, maxSag)
        length = get_value(ratioLength * size, maxLength)
        algo.tessellate(group, sag, length, maxAngle,\
            createNormals, uvMode, uvChannel, uvPadding, createTangents,\
            createFreeEdges, keepBRepShape, overrideExistingTessellation)
    core.addConsoleVerbose(2)
    core.popProgression()